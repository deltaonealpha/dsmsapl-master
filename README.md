#OSSRTDS
# REGISTRY: deltaSTOREMANAGER
## by Pranav Balaji

This repository contains the code for dsmapl
open-source billing system. Can be used by anyone until you don't copy my hardwork hehe.

## Installation Instructions:
. Fork master repo from: deltaonealpha.github.io/dsmsapl
. Install ALL required libraries from PyPi:
    -_- mysql.connector
    -_- coloroma
    -_- os
    -_- time
    -_- datetime
    -_- opencv-python (cv2)
. Run login script or use shortcut (if included in the repo)
. Use anaconda to debug if facing localised installation erors

## Changelog (version-specific):
### v3.1 ALPHA:

#### > zconf terminal introduced for echo-less admin password input and complete log-file updation.

#### > Added an option for adverts (default: "Hotel? TRIVAGO!").

#### > Includes native anaconda support now.

#### > Added boot-time flash screen and prompt. 

#### > Custom customer ID option removed. Automatic ID system implemented in-place.

#### > Presenting main password lock and user-name input with (username) echo in main script.

#### > NEW: Added script for login-bypass prevention. Using new bridge framework for anti-snooping measures.

#### > Encryption option made available. Tinker-around to enable AES encryption now.

#### > Code-bobs included for new tax-bar system with custom tax brackets. Will be enabled in future revisions.

#### > Optimized load-timings and the UI, reduced delays and corrected language in certain fields. 

#### > Presenting the new (under testing) site for the code: www.dsmsapl.bss.design



### https://deltaonealpha.github.io/.home/
-------Website currently under development-------
### dsmsapl


## Development Notice(s):
#### Version 3.1 ALPHA now released on GitHub.
#### Now includes FULL QUALITY printables on the repo through external OneDrive link.

CREATED BY PRANAV BALAJI
(@deltaonealpha)

